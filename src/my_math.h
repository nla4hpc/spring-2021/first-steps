/**
@file my_math.h
*/

#ifndef MY_MATH_H_INCLUDED
#define MY_MATH_H_INCLUDED



/** @brief add_numbers

    This function adds two numbers f1 and f2, and returns the result f1+f2.
    The original code is from: https://github.com/pothitos/gtest-demo-gitlab.git
    
    @author H. Anzt, KIT
    @date March 2019
    */
double add_numbers(const double f1, const double f2);


/** @brief add_numbers

    This function subracts f2 from numbers f1 and returns the result f1-f2.
    The original code is from: https://github.com/pothitos/gtest-demo-gitlab.git
    
    @author H. Anzt, KIT
    @date March 2019
    */
double subtract_numbers(const double f1, const double f2);


/** @brief multiply_numbers

    This function multiplies two numbers f1 and f2, 
    and returns the result f1*f2.
    The original code is from: https://github.com/pothitos/gtest-demo-gitlab.git
    
    @author H. Anzt, KIT
    @date March 2019
    */
double multiply_numbers(const double f1, const double f2);


#endif /* MY_MATH_H_INCLUDED */
